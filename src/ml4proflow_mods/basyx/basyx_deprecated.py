# type: ignore
# known bugs
# refactor url->machine_url
# refactor registry/url

import requests
from IPython.display import JSON
from requests.auth import HTTPBasicAuth
import ipywidgets as widgets


class Basyx:
    def __init__(self, machine_url, smart_services_url, user, password, verify=True):
        self.url = machine_url
        self.smart_services_url = smart_services_url
        self.user = user
        self.password = password
        self.session = requests.Session()
        self.session.auth = HTTPBasicAuth(self.user, self.password)
        self.session.verify = verify

    def parse_bom(self):
        if 'bom' in self.__dict__.keys():
            return self.bom
        self.bom = {}
        reg = self.get_registry()
        for aas in reg.data:
            bom_submodel = [x for x in aas['submodels'] if x['idShort'] == 'BoM']
            if len(bom_submodel) > 0:
                _endpoints = bom_submodel[0]['endpoints']
                if len(_endpoints) == 0:
                    break
                _endpoint_addr = _endpoints[0]['address']
                _bom = self.get_low_low_level(_endpoint_addr)  # todo: create new get endpoint func
                childs = [x['idShort'].lower() for x in _bom.data['submodelElements']
                          if x['modelType']['name'] == 'Entity']
                self.bom[aas['idShort'].lower()] = childs
        return self.bom

    def get_asset_img(self, idShort):
        aas_s = [d for d in self.get_registry().data if d['idShort'] == idShort]
        if len(aas_s) != 1:
            raise ValueException("No AAS with id: %s" % idShort)
        aas = aas_s[0]
        aasid_s = [a for a in aas['submodels'] if a['idShort'] == 'AssetIdentification']
        if len(aasid_s) == 0:
            raise ValueException(f"AAS {aas} has no asset identification!")

        img_url_pre = self.get_low_low_level(f'{aasid_s[0]["endpoints"][0]["address"]}/submodelElements/assetPicture')
        try:
            img_url = "https://research.lenze.com:9443/%s" % img_url_pre.data['value']
            result = self.session.get(img_url)
            return result.content
        except:
            print("%s no img (pre: %s)" % (img_url, img_url_pre.data))
            return None
        return img_url

    def __check_status_code(self, result):
        if result.status_code != 200:
            raise NameError('Status code is %d' % result.status_code)

    def get_smart_services_registry(self):
        result = self.session.get('%s/registry/api/v1/registry' % self.smart_services_url)
        self.__check_status_code(result)
        return JSON(result.json())  # todo: check more json fields

    def get_registry(self):  # registry
        result = self.session.get('%s/api/v1/registry' % self.url)
        self.__check_status_code(result)
        return JSON(result.json())  # todo: check more json fields

    def get_submodels(self, aaid):
        result = self.session.get('%s/api/v1/registry/%s/submodels' % (self.url, aaid))
        self.__check_status_code(result)
        return JSON(result.json())  # todo: check more json fields

    def get_submodel(self, aaid, submodel):
        result = self.session.get('%s/%s/aas/submodels/%s' % (self.url, aaid, submodel))
        self.__check_status_code(result)
        return JSON(result.json())  # todo: check more json fields

    def get_data_elements_from_submodel(self, aaid, submodel):
        result = self.session.get('%s/%s/aas/submodels/%s/dataElements' % (self.url, aaid, submodel))
        self.__check_status_code(result)
        return JSON(result.json())  # todo: check more json fields

    def get_value_from_submodel(self, aaid, submodel, element_id, exception_value='dummy'):
        result = self.session.get(f'{self.url}/{aaid}/aas/submodels/{submodel}/dataElements/{element_id}/value')
        if exception_value == 'dummy':
            self.__check_status_code(result)
        else:
            if result.status_code != 200:
                return exception_value
        return result.json()['value']  # todo: check more json fields

    def filter_aaid_by_submodel(self, model_id):
        result = []
        tmp = self.get_registry()
        for aaid in tmp.data:
            for model in aaid['submodels']:
                if model['idShort'] == model_id:
                    result.append(aaid['idShort'])
        return result

    def filter_aas_by_submodel(self, model_id):
        result = []
        tmp = self.get_registry()
        for aaid in tmp.data:
            for model in aaid['submodels']:
                if model['idShort'] == model_id:
                    result.append(aaid)
        return result

    def filter_aas_by_submodel_old(self, model_id):
        result = []
        tmp = self.get_registry()
        for aaid in tmp.data:
            for model in aaid['submodels']:
                if model['idShort'] == model_id:
                    result.append(aaid['identification']['id'])
        return result

    def print_registry(self):
        result = self.get_registry()
        for shell in result:
            print('%8s (%s)' % (shell['idShort'], shell['identification']['id']))
            print('\tEndpoints:')
            for endpoint in shell['endpoints']:
                print('\t\t%20s' % endpoint['address'])
            print('\tSubmodels:')
            for submodel in shell['submodels']:
                print('\t\t%8s' % submodel['idShort'])

    def print_submodeles(self, aaid):
        result = self.get_submodels(aaid)
        for submodul in result:
            print('%s (%s)' % (submodul['idShort'], submodul['identification']['id']))
            print('\tEndpoints:')
            for endpoint in submodul['endpoints']:
                print('\t\t%20s' % endpoint['address'])

    def print_submodel(self, aaid, submodel):
        result = self.get_submodel(aaid, submodel)
        print("Category: %s" % result['category'])
        print("Kind: %s" % result['kind'])
        print("Description:")
        for description in result['description']:
            for descriptionKey, descriptionValue in description.items():
                print('\t %s: %s' % (descriptionKey, descriptionValue))

    def recover_idShort(self, idShort):
        idShort = idShort.lower()
        _tmp = [d['idShort'] for d in self.get_registry().data if d['idShort'].lower() == idShort]
        if len(_tmp) == 0:
            raise NameError('idShort: "%s" not found!' % idShort)
        return _tmp[0]

    def get_aas_by_idShort(self, idShort):
        _tmp = [d for d in self.get_registry().data if d['idShort'] == idShort]
        if len(_tmp) == 0:
            raise NameError('idShort: "%s" not found!' % idShort)
        return JSON(_tmp[0])

    def get_urn_from_idShort(self, idShort):  # todo: upper/lower characters?
        _tmp = [d['identification']['id'] for d in self.get_registry().data if d['idShort'] == idShort]
        if len(_tmp) == 0:
            raise NameError('idShort: "%s" not found!' % idShort)
        return _tmp[0]

    def get_asset_type(self, aas_json):
        technical_data = [a for a in aas_json.data['submodels'] if a['idShort'] == 'TechnicalData']
        if len(technical_data) == 0:
            raise ValueError("AAS: '%s' has no technical data" % aas_json.data)
        return self.get_low_low_level("%s/submodelElements/assettype" % technical_data[0]["endpoints"][0]["address"])

    def get_assed_administration_shell_by_id(self, id):
        result = self.session.get('%s/api/v1/registry' % self.url)
        return result

    def get_low_level(self, call):
        result = self.session.get("%s/%s" % (self.url, call))
        return JSON(result.json())

    def get_low_low_level(self, call):
        result = self.session.get("%s" % (call))
        try:
            return JSON(result.json())
        except:  # todo handle 404
            print("%s error" % call)
            return JSON([])

    def get_assed_administration_shell_ids(self):
        test = self.get_assed_administration_shells(self)
        return [shell['idShort'] for shell in test['entity']]

    def draw_as_graph(self):
        from graphviz import Graph
        dot = Graph(engine="circo")
        dot.graph_attr['mindist'] = ".0"
        dot.graph_attr['size'] = "10,20"
        dot.node('AAS', 'AAS')
        reg = self.get_registry()
        for aas in reg.data:
            aas_txt = "%s" % (aas['idShort'])
            dot.node(aas['idShort'], aas_txt)
            dot.edge('AAS', aas['idShort'])
        return dot

    def draw_as_hierarchy_graph(self):
        from graphviz import Graph
        dot = Graph(engine="neato")  # circo
        dot.graph_attr['mindist'] = ".0"
        dot.graph_attr['fontsize'] = "12"
        dot.graph_attr['size'] = "10,10"
        dot.graph_attr['sep'] = "0.1"
        dot.graph_attr['overlap'] = "scale"
        dot.graph_attr['splines'] = "true"
        reg = self.get_registry()
        bom = self.parse_bom()
        for aas in reg.data:
            aas_txt = "%s" % (aas['idShort'])
            dot.node(aas['idShort'].lower(), aas_txt)
            aas_name = aas['idShort'].lower()
            if aas_name in bom:
                childs = bom[aas_name]
                for c in childs:
                    dot.edge(aas['idShort'].lower(), c.lower())
        return dot

    def _on_connect_update(self, button):
        self.url = self._machine_registry.value
        self.smart_services_url = self._smart_services_regestry.value
        if 'bom' in self.__dict__.keys():
            del self.__dict__['bom']
        self._on_connect(button)

    def create_gui(self, on_connect):
        _width = '500px'
        self._on_connect = on_connect
        self._machine_registry = widgets.Text(
                value=self.url,
                placeholder='',
                description='Machine Registry:',
                disabled=False,
                layout=widgets.Layout(width=_width)
            )
        self._smart_services_regestry = widgets.Text(
                value=self.smart_services_url,
                placeholder='',
                description='Smart Services Registry:',
                disabled=False,
                layout=widgets.Layout(width=_width)
            )
        self._user = widgets.Text(
                value='publicuser',
                placeholder='',
                description='Nutzer:',
                disabled=False,
                layout=widgets.Layout(width=_width)
            )
        self._pw = widgets.Password(
                value='FTe4yzzDdjnzkTSP',
                placeholder='',
                description='Passwort:',
                disabled=False,
                layout=widgets.Layout(width=_width)
            )
        self._connect = widgets.Button(
            description='Verbinden',
            disabled=False,
            button_style='',  # 'success', 'info', 'warning', 'danger' or ''
        )
        self._connect.on_click(self._on_connect_update)
        _connection = widgets.VBox([self._machine_registry,
                                    self._smart_services_regestry,
                                    self._user,
                                    self._pw,
                                    self._connect],
                                   layout=widgets.Layout(width='100%'))
        return _connection
