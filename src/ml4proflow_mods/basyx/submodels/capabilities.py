def check_semantic_id(x: dict, semanticId: str) -> None:
    dummy = x['semanticId']['keys'][0]['value']
    assert dummy == semanticId, f'Is "{dummy}" but should be "{semanticId}"'


def parse_capabilities(sm: dict) -> dict:
    result = {}
    check_semantic_id(sm, 'https://wiki.eclipse.org/BaSyx_/_Documentation_/_Submodels_/_Capability')
    for capabilities_set in sm['submodelElements']:
        check_semantic_id(capabilities_set, 'https://wiki.eclipse.org/BaSyx_/_Documentation_/_Submodels_/_Capability#CapabilitySet')  # noqa:E501
        for capabilities_container in capabilities_set['value']:
            check_semantic_id(capabilities_container, 'https://wiki.eclipse.org/BaSyx_/_Documentation_/_Submodels_/_Capability#CapabilityContainer')  # noqa:E501
            cap_name = capabilities_container["idShort"]
            result[cap_name] = {}
            for tmp in capabilities_container['value']:
                if tmp['semanticId']['keys'][0]['value'] == 'https://wiki.eclipse.org/BaSyx_/_Documentation_/_Submodels_/_Capability#PropertySet':  # noqa:E501
                    for property_c in tmp['value']:
                        check_semantic_id(property_c, 'https://wiki.eclipse.org/BaSyx_/_Documentation_/_Submodels_/_Capability#PropertyContainer')  # noqa:E501
                        for p in property_c['value']:
                            if p['semanticId']['keys'][0]['value'] == 'https://wiki.eclipse.org/BaSyx_/_Documentation_/_Submodels_/_Capability#Property':  # noqa:E501
                                result[cap_name][p['idShort']] = p['value']
    return result
