from ml4proflow_mods.basyx.submodels.hierarchical_structures import parse_hierarchicalstructures


def decode_submodel(sm):
    if sm['semanticId']['keys'][0]['value'] == 'https://admin-shell.io/idta/HierarchicalStructures/1/0/Submodel':
        return parse_hierarchicalstructures(sm)
