def check_semantic_id(x: dict, semanticId: str) -> None:
    dummy = x['semanticId']['keys'][0]['value']
    assert dummy == semanticId, f'Is "{dummy}" but should be "{semanticId}"'


# this needs to be rewritten: it ignores all rel!
def decode_entity(e: dict) -> dict:
    result = {}
    for tmp in e['statements']:
        if tmp['semanticId']['keys'][0]['value'] == 'https://admin-shell.io/idta/HierarchicalStructures/Node/1/0':
            result[tmp['asset']['keys'][0]['value']] = decode_entity(tmp)
    return result


def parse_hierarchicalstructures(sm: dict) -> dict:
    check_semantic_id(sm, 'https://admin-shell.io/idta/HierarchicalStructures/1/0/Submodel')
    sm_lvl0 = {t['idShort']: t for t in sm['submodelElements']}
    assert sm_lvl0["ArcheType"]["value"] == "Down", f'ArcheType is unsupported: {sm_lvl0["ArcheType"]["value"]}'
    tmp = decode_entity(sm_lvl0["EntryNode"])
    return {sm_lvl0["EntryNode"]['asset']['keys'][0]['value']: tmp}
