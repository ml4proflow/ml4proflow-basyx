from basyx.aas.adapter.json.json_serialization import AASToJsonEncoder
import json
import requests
from requests.auth import AuthBase
import urllib
from typing import Optional, Generator
from basyx.aas.model.aas import AssetAdministrationShell
from basyx.aas.model.submodel import Submodel
from basyx.aas.model import base


class SimpleJSONAASParser:
    @staticmethod
    def get_semantic_ids_from_submodels(submodels: list[dict] | dict) -> Generator:
        if type(submodels) is not list:
            submodels = [submodels]
        for x in submodels:
            assert x['modelType']['name'] == 'SubmodelDescriptor'
            yield x['semanticId']['keys']

    @staticmethod
    def get_aas_identifications_from_registry(registry: list) -> Generator:
        for x in registry:
            assert x['modelType']['name'] == 'AssetAdministrationShellDescriptor'
            yield x['identification']


class BasyxRegistryConnector:
    def __init__(self, server_url: str, auth: Optional[AuthBase] = None, verify: bool = True) -> None:
        self.server_url = server_url
        self.url = f'{self.server_url}/registry/api/v1/registry'
        self.auth = auth
        self.session = requests.Session()
        self.session.auth = auth
        self.session.verify = verify

    def __check_status_code(self, result: requests.Response) -> None:
        if result.status_code != 200:
            print(result.__dict__)
            raise NameError('Status code is %d' % result.status_code)

    def __parse_identification(self, aas_identification: dict | str) -> str:
        if type(aas_identification) is dict:
            aas_id = aas_identification['id']
        else:
            aas_id = aas_identification
        if '/' in aas_id:
            aas_id = urllib.parse.quote(aas_id, safe="")
        return aas_id

    def get_registry(self) -> dict:
        result = self.session.get(self.url)
        self.__check_status_code(result)
        return result.json()

    def get_submodels(self, aas_identification: dict | str) -> dict:
        aas_id = self.__parse_identification(aas_identification)
        r = self.session.get(f'{self.url}/{aas_id}/submodels')
        self.__check_status_code(r)
        return r.json()


class BasyxAASShellConnector:
    def __init__(self, server_url: str, auth: Optional[AuthBase] = None, verify: bool = True) -> None:
        self.url = server_url
        self.auth = auth
        self.session = requests.Session()
        self.session.auth = auth
        self.session.headers.update({"Content-Type": "application/json"})
        self.session.verify = verify

    def __check_status_code(self, result: requests.Response) -> None:
        if result.status_code != 200:
            print(result.__dict__)
            raise NameError('Status code is %d' % result.status_code)

    def _parse_identification(self, aas_identification: dict | str) -> str:
        if type(aas_identification) is dict:
            aas_id = aas_identification['id']
        else:
            aas_id = aas_identification
        if '/' in aas_id:
            aas_id = urllib.parse.quote(aas_id, safe="")
        return aas_id

    def put_aas(self, aas: AssetAdministrationShell, sms: list[base.AASReference[Submodel]]):
        aas_json_str = json.dumps(aas, cls=AASToJsonEncoder)
        aas_id = self._parse_identification(aas.identification.id)
        response = self.session.put(f'{self.url}/aasServer/shells/{aas_id}', data=aas_json_str)
        assert response.status_code == 200, f'PUT AAS failed with {response.text}'
        for sm in sms:
            sm_json_str = json.dumps(sm, cls=AASToJsonEncoder, indent=2)
            response = self.session.put(f'{self.url}/aasServer/shells/{aas_id}/aas/submodels/{sm.id_short}',
                                        data=sm_json_str)
            assert response.status_code == 200, f'PUT SM failed with {response.text}'
            print(response.text)

    def put_file(self, filepath, aas_id, path):
        aas_id = self._parse_identification(aas_id)
        p = f'{self.url}/aasServer/shells/{aas_id}/{path}'
        with open(filepath, "rb") as f:
            f = {'file': ("test.png", f, "image/png")}
            response = requests.post(p, files=f)
            assert response.status_code == 201, f'Post file failed with {response.text}'
        return p

    def get_submodel(self, aas_identification: dict | str, submodel_id_short: str) -> dict:
        aas_id = self._parse_identification(aas_identification)
        r = self.session.get(f'{self.url}/aasServer/shells/{aas_id}/aas/submodels/{submodel_id_short}/submodel')
        self.__check_status_code(r)
        return r.json()
